export const concat = (a: any[], b: any[]) => [...a, ...b];

export const concatAdvanced = (...concatArrays: any[][]) => {
    return concatArrays.reduce((prev, current) => {
        prev.push(...current);
        return prev;
    });
};