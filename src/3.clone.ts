// Shallow
export function clone(source) {
    return {...source};
}

// Deep
export function deepClone(source) {
    return Object.keys(source).reduce((prev, key) => {
        if(typeof source[key] == "object") {
            prev[key] = deepClone(source[key]);
        } else {
            prev[key] = source[key];
        }
        return prev;
    }, {});
}

export function merge(source, target) {
    return {...target, ...source};
}