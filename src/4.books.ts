interface Book {
    title: string;
    isRead: boolean;
}

export function isBookRead(books: Book[], titleToSearch: string) {
    // Se puede usar find y luego meterle dos exclamaciones para forzar un valor booleano pero creo que para 
    // esta funcion tampoco hace falta eso, con un some basta, aunque tenga un edge case que podria romperlo
    return books.some(book => book.title == titleToSearch && book.isRead);
}