export const head = ([first, ...others]) => first;

export const tail = ([first, ...others]) => others;

export const init = (arr: any[]) => arr.slice(0, arr.length - 1);

export const last = (arr: any[]) => arr.pop();
