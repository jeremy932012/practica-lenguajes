// Jest doesn't implement crypto API so we need to add a package for that
import * as crypto from "@trust/webcrypto";

export class SlotMachine {
    private contador = 0;

    public play() {
        this.contador++;
        const arr = crypto.getRandomValues(new Uint8Array(3));
        // Estos un array de 8 bites por lo tanto tenemos un maximo de 0-255 numeros posibles
        // ya que solo pilla los numeros naturales porque es un uint además
        // como lo pasamos a booleano lo dividimos en dos y lo aproximamos a un entero de 127
        // Obviamente estadisticamente tendras mas posibilidades de ganar que de perder por 0.5 pero
        // bueno nada es perfecto en esta vida, ni siquiera la aleatoriedad simulada :P
        const hasGanado = arr.every(num => num > 127);
        if(!hasGanado) return console.log("Good luck next time!!"); // Raro devolver un console.log pero no quiero crear bracket
        console.log(`Congratulations!!!. You won ${this.contador} coins!!`);
        this.contador = 0;
    }
}