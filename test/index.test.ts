import { head, tail, init, last } from "../src/1.array"
import { concat, concatAdvanced } from "../src/2.concat"
import { clone, merge, deepClone } from "../src/3.clone"
import { isBookRead } from "../src/4.books"
import { SlotMachine } from "../src/5.slot"

describe("1. Array", () => {
    test("head", () =>  {
        expect(head([1,2,3,4,5])).toEqual(1)
    })
    test("tail", () =>  {
        expect(tail([1,2,3,4,5])).toEqual([2,3,4,5])
    })
    test("init", () =>  {
        expect(init([1,2,3,4,5])).toEqual([1,2,3,4])
    })
    test("last", () =>  {
        expect(last([1,2,3,4,5])).toEqual(5)
    })
})

describe("2. Concat", () => {
    test("concat", () =>  {
        expect(concat([1,2,3,4,5],[6,7,8])).toEqual([1,2,3,4,5,6,7,8])
    })
    test("concatAdvanced", () =>  {
        expect(
            concatAdvanced([1,2,3,4,5],[6,7,8],[9,10],[11,12,13]))
            .toEqual([1,2,3,4,5,6,7,8,9,10,11,12,13])
    })
})

describe("3. Clone", () => {
    test("clone", () =>  {
        const source = {title: "Harry Potter", isRead: false};
        expect(clone(source)).not.toBe(source)
        expect(clone(source)).toEqual(source)
    })
    test("deepClone", () =>  {
        const source = {title: "Harry Potter", isRead: false, autor: {name: "J.K Rowling", Anio: 1972} };
        expect(deepClone(source)).not.toBe(source)
        expect(deepClone(source)).toEqual(source)
    })
    test("merge", () =>  {
        const target = {name: "Maria", age: 31, 
            married: true, surname: "Ibañez", country: "SPA"};
        const a = { name: "Maria", surname: "Ibañez", country: "SPA" };
        const b = { name: "Luisa", age: 31, married: true };

        expect(merge(a, b)).toEqual(target)
    })
})

describe("4. Books", () => {
    test("isBookRead", () =>  {
        const books = [
            { title: "Harry Potter y la piedra filosofal", isRead: true },
            { title: "Canción de hielo y fuego", isRead: false },
            { title: "Devastación", isRead: true },
          ];
        expect(isBookRead(books, "Devastación")).toEqual(true)
        expect(isBookRead(books, "Canción de hielo y fuego")).toEqual(false)
        expect(isBookRead(books, "Los Pilares de la Tierra")).toEqual(false)
    })
})

describe("5. Slot", () => {
    test("Jugando como un ludopata 😁", () =>  {
        const newSlot = new SlotMachine();
        newSlot.play()
        newSlot.play()
        newSlot.play()
        newSlot.play()
        newSlot.play()
    })
})